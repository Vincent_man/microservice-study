import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:

        def process_approvals(ch, method, properties, body):
            content = json.loads(body)
            send_mail(
                "Your presentation has been approved",
                f"{content['presenter_name']} we're happy to tell you that your presentation {content['title']} has been accepted",
                "admin@conference.go",
                [content["presenter_email"]],
                fail_silently=False,
            )

        def process_rejections(ch, method, properties, body):
            content = json.loads(body)
            send_mail(
                "Your presentation has been rejected",
                f"{content['presenter_name']} we're sorry to tell you that your presentation {content['title']} has been rejected",
                "admin@conference.go",
                [content["presenter_email"]],
                fail_silently=False,
            )

        #  def process_approval(ch, method, properties, body):
        #     content = json.loads(body)
        #     send_mail(
        #         "Your presentation has been accepted",
        #         f"{content['presenter_name']}, we're happy to tell you that your presentation {content['title']} has been accepted",
        #         "admin@conference.go",
        #         [content["presenter_email"]],
        #         fail_silently=False,
        #     )

        # def process_rejection(ch, method, properties, body):
        #     content = json.loads(body)
        #     send_mail(
        #         "Your presentation has been rejected",
        #         f"{content['presenter_name']}, we're sad to inform you that your presentation {content['title']} has been rejected",
        #         "admin@conference.go",
        #         [content["presenter_email"]],
        #         fail_silently=False,
        #     )

        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)

        channel = connection.channel()
        channel.queue_declare(queue="presentation_approvals")
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approvals,
            auto_ack=True,
        )

        channel.queue_declare(queue="presentation_rejections")
        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_rejections,
            auto_ack=True,
        )
        print("pls fucking work")
        channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)

# def main():
# # Just extra stuff to do when the script runs
# if __name__ == "__main__":
#     try:
#         main()
#     except KeyboardInterrupt:
#         print("Interrupted")
#         try:
#             sys.exit(0)
#         except SystemExit:
#             os._exit(0)
# import pika, sys, os  # Import stuff that will be used


# Create a function that will process the message when it arrives
# def process_message(ch, method, properties, body):
#     print("  Received %r" % body)


# Create a main method to run
# def main():
#     # Set the hostname that we'll connect to
#     parameters = pika.ConnectionParameters(host="rabbitmq")

#     # Create a connection to RabbitMQ
#     connection = pika.BlockingConnection(parameters)

#     # Open a channel to RabbitMQ
#     channel = connection.channel()

#     # Create a queue if it does not exist
#     channel.queue_declare(queue="process_approvals")

#     # Configure the consumer to call the process_message function
#     # when a message arrives
#     channel.basic_consume(
#         queue="process_approvals",
#         on_message_callback=process_message,
#         auto_ack=True,
#     )

#     # Print a status
#     print(" [*] Waiting for messages. To exit press CTRL+C")

#     # Tell RabbitMQ that you're ready to receive messages
#     channel.start_consuming()


# send_mail(
#     'Subject here',
#     'Here is the message.',
#     'from@example.com',
#     ['to@example.com'],
#     fail_silently=False,
# )

# def process_message(ch, method, properties, body):
#     print("  Received %r" % body)
#     parameters = pika.ConnectionParameters(host='rabbitmq')
#     connection = pika.BlockingConnection(parameters)
#     channel = connection.channel()
#     channel.queue_declare(queue='tasks')
#     channel.basic_consume(
#         queue='tasks',
#         on_message_callback=process_message,
#         auto_ack=True,
#     )
#     channel.start_consuming()
